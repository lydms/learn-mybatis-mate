
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for test_account
-- ----------------------------
DROP TABLE IF EXISTS `test_account`;
CREATE TABLE `test_account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) DEFAULT NULL COMMENT '账号ID',
  `account` varchar(200) DEFAULT NULL COMMENT '账号',
  `token` varchar(500) DEFAULT NULL COMMENT 'token',
  `language` varchar(20) DEFAULT NULL COMMENT '语言',
  `password` varchar(200) DEFAULT NULL COMMENT '密码',
  `deleted` bit(1) DEFAULT b'0' COMMENT '是否被删除(0:未删除。1:已删除)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test_account
-- ----------------------------
BEGIN;
INSERT INTO `test_account` VALUES (1, 9223372036854775807, 'zhangsan', 'kjgt8urWdf15xa6QdTDjdhfLAstSL2NlIwHGtBO7XApixE2sIogNTZsL32Zkm1VG', 'cn', 'AK54MoLAe5SyDbHzMV4CNiOpkmwnTJGW', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (2, 9223372036854775807, 'lisi', 'M4E8lZnpq1SmkznRmBdJoh2wGKFAOIjOCn6dZee7B4S3vKXTjkL4rXC1zOFVOe3m', 'cn', 'z8VOzdm1DdnJL1izduxZ6qTaBVCs0dUS', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (3, 9223372036854775807, 'wangwu', 'DiNKPqDIepDO8ES8rjuRAWoZEcUZiXQMSOrcdMG0QNJyojlJ2UBuPKvykrbPF6Aw', 'cn', 'tdPg9B9OlNbF3EPvBzV4Sb1cpj9KPUI7', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (4, 9223372036854775807, 'zhaoliu', 'SU8bLZJ1r8WhLzimhg5ViRnbMrlPIFmoZfANdKnJYoacahBL6mB6QpBeTSK0woSA', 'cn', 'uQW2M4Wu4FykIo2K3GMuxp07DEEszmkH', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (5, 9223372036854775807, 'tom', '27BKcNoMtqOmVCOBZshEdD77eyQSkOdlyMwIO4vwrgVS6ftDsUXiMrp2LyuSFL6g', 'cn', '8CxObRSajl2G5kWx2JdcbOjo87p3BvKC', b'0', '2022-01-29 10:18:38', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (6, 9223372036854775807, 'mybatis', 'xnnwhLfEqaj8n58nz6bTsOiTJg1UEUZfDXdpyucKYMg4q3XXoJnYYxhz5wli01yE', 'cn', 'lUaBslGpNim7IbrYtkA6jcTAwpiMMa6z', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (7, 9223372036854775807, 'mybatis-plus', 't9MnneeoLvQevlmVO1Mfb0Ftz5u2pD2ty96EdbEQ6QCcknHphffXXT7oOpmaxIC1', 'cn', 'QyBxNVQXJMdwafM1nkca1Y2D9nAXIH2V', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (8, 9223372036854775807, 'test01', '6X2fJjVsKpBf7sgp8s9t0WTTX77LSiOqHHjCcBo4paupgR7by5dm9mSTViEXdL3q', 'cn', 'j1KULWvKkUwG9oCL6WlCn3EPn8DGgKUc', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (9, 9223372036854775807, 'test02', 'plcemthh3vBziVqVF3YX2H6ksmGrnYBd7eWKBNJ3l4g8iXHeGAweFBdklHJf7vrh', 'cn', '1VDyuVzbbLJ5Ul5Gu9qgFur8EbkKMnmE', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (10, 9223372036854775807, 'test03', 'JTr6PirLn6qJs4tv6yUZmOaci49dpOYaYhCqlxuuf2ucJxUCtxOsPAvBuh4SMoW9', 'cn', '6hUOZIAcvMWomWzZesorPKbqZj7TvMkU', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (11, 9223372036854775807, 'test04', 'Vw3dSKMsRsS3vyg9j2zolCfzTkxz6ABLYAZIvFZpnPeYjWaDNq8Am2oWzcrWqVG1', 'cn', 'IBSK2NHSTBNPZw67sH3tBNh2bh6uSzKI', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
INSERT INTO `test_account` VALUES (12, 9223372036854775807, 'test05', 'AmwmEhstvKP5saqlnPy45JpB8b0aWXZWkFWhyht4nFLMSsEprYgZuOuwSDO1I2sZ', 'cn', '6eNEM1taNCnWBHG8uvKwM6Z1rQH1guxb', b'0', '2022-01-28 11:12:12', '2022-01-28 11:12:12');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
