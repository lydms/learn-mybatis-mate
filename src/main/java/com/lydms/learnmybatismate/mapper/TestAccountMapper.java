package com.lydms.learnmybatismate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lydms.learnmybatismate.entity.TestAccount;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lydms
 * @since 2022-01-28
 */

public interface TestAccountMapper extends BaseMapper<TestAccount> {



}
