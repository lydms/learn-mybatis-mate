package com.lydms.learnmybatismate.controller;


import com.lydms.learnmybatismate.entity.TestAccount;
import com.lydms.learnmybatismate.service.ITestAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lydms
 * @since 2022-01-28
 */
@RestController
@RequestMapping("/system/account")
public class TestAccountController {
    @Autowired
    private ITestAccountService accountService;

    @RequestMapping(value = "/test")
    public Object getArtSpeakByPage() {
        List<TestAccount> list = accountService.list();
        System.out.println(list.size());
        return list;
    }
}
