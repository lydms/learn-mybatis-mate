package com.lydms.learnmybatismate.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lydms.learnmybatismate.entity.TestAccount;
import com.lydms.learnmybatismate.mapper.TestAccountMapper;
import com.lydms.learnmybatismate.service.ITestAccountService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lydms
 * @since 2022-01-28
 */
@Service
public class TestAccountServiceImpl extends ServiceImpl<TestAccountMapper, TestAccount> implements ITestAccountService {

}
