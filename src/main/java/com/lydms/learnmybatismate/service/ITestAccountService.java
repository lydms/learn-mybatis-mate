package com.lydms.learnmybatismate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lydms.learnmybatismate.entity.TestAccount;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lydms
 * @since 2022-01-28
 */
public interface ITestAccountService extends IService<TestAccount> {


}
