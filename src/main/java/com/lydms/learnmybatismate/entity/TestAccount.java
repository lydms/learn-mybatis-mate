package com.lydms.learnmybatismate.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lydms
 * @since 2022-01-28
 */
@Getter
@Setter
@TableName("test_account")
public class TestAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 账号ID
     */
    @TableField("account_id")
    private Long accountId;

    /**
     * 账号
     */
    @TableField("account")
    private String account;

    /**
     * token
     */
    @TableField("token")
    private String token;

    /**
     * 语言
     */
    @TableField("language")
    private String language;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 是否被删除(0:未删除。1:已删除)
     */
    @TableField("deleted")
    private Boolean deleted;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;


}
